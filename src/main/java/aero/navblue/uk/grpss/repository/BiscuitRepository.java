package aero.navblue.uk.grpss.repository;

import aero.navblue.uk.grpss.domain.Biscuit;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Biscuit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BiscuitRepository extends JpaRepository<Biscuit, Long> {

    @Modifying
    @Query(value = "delete FROM biscuit WHERE id = ?1", nativeQuery = true)
    void nativeDelete(Long id);

}
