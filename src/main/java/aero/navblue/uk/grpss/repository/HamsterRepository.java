package aero.navblue.uk.grpss.repository;

import aero.navblue.uk.grpss.domain.Hamster;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Hamster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HamsterRepository extends JpaRepository<Hamster, Long>, HamsterRepositoryCustom {


//    @Modifying
//    @Query(value = "delete FROM hamster WHERE id = ?1", nativeQuery = true)
//    void nativeDelete(Long id);

}
