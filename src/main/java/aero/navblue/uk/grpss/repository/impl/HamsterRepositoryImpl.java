package aero.navblue.uk.grpss.repository.impl;

import aero.navblue.uk.grpss.domain.Hamster;
import aero.navblue.uk.grpss.repository.HamsterRepositoryCustom;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class HamsterRepositoryImpl implements HamsterRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void nativeDelete(Long id) {
        Query query = entityManager.createNativeQuery("DELETE FROM hamster WHERE id = :id");
        query.setParameter("id", id);
        query.unwrap(NativeQuery.class).addSynchronizedEntityClass(Hamster.class);
        query.executeUpdate();
    }
}
