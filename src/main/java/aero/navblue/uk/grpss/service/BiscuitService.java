package aero.navblue.uk.grpss.service;

import aero.navblue.uk.grpss.domain.Biscuit;
import aero.navblue.uk.grpss.repository.BiscuitRepository;
import aero.navblue.uk.grpss.service.dto.BiscuitDTO;
import aero.navblue.uk.grpss.service.mapper.BiscuitMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Biscuit.
 */
@Service
@Transactional
public class BiscuitService {

    private final Logger log = LoggerFactory.getLogger(BiscuitService.class);

    private final BiscuitRepository biscuitRepository;

    private final BiscuitMapper biscuitMapper;

    public BiscuitService(BiscuitRepository biscuitRepository, BiscuitMapper biscuitMapper) {
        this.biscuitRepository = biscuitRepository;
        this.biscuitMapper = biscuitMapper;
    }

    /**
     * Save a biscuit.
     *
     * @param biscuitDTO the entity to save
     * @return the persisted entity
     */
    public BiscuitDTO save(BiscuitDTO biscuitDTO) {
        log.debug("Request to save Biscuit : {}", biscuitDTO);
        Biscuit biscuit = biscuitMapper.toEntity(biscuitDTO);
        biscuit = biscuitRepository.save(biscuit);
        return biscuitMapper.toDto(biscuit);
    }

    /**
     * Get all the biscuits.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<BiscuitDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Biscuits");
        return biscuitRepository.findAll(pageable)
            .map(biscuitMapper::toDto);
    }

    /**
     * Get one biscuit by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public BiscuitDTO findOne(Long id) {
        log.debug("Request to get Biscuit : {}", id);
        Biscuit biscuit = biscuitRepository.findOne(id);
        return biscuitMapper.toDto(biscuit);
    }

    /**
     * Delete the biscuit by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Biscuit : {}", id);
        biscuitRepository.nativeDelete(id);
    }
}
