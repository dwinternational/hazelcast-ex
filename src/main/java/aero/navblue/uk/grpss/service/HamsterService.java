package aero.navblue.uk.grpss.service;

import aero.navblue.uk.grpss.domain.Hamster;
import aero.navblue.uk.grpss.repository.HamsterRepository;
import aero.navblue.uk.grpss.service.dto.HamsterDTO;
import aero.navblue.uk.grpss.service.mapper.HamsterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Hamster.
 */
@Service
@Transactional
public class HamsterService {

    private final Logger log = LoggerFactory.getLogger(HamsterService.class);

    private final HamsterRepository hamsterRepository;

    private final HamsterMapper hamsterMapper;

    public HamsterService(HamsterRepository hamsterRepository, HamsterMapper hamsterMapper) {
        this.hamsterRepository = hamsterRepository;
        this.hamsterMapper = hamsterMapper;
    }

    /**
     * Save a hamster.
     *
     * @param hamsterDTO the entity to save
     * @return the persisted entity
     */
    public HamsterDTO save(HamsterDTO hamsterDTO) {
        log.debug("Request to save Hamster : {}", hamsterDTO);
        Hamster hamster = hamsterMapper.toEntity(hamsterDTO);
        hamster = hamsterRepository.save(hamster);
        return hamsterMapper.toDto(hamster);
    }

    /**
     * Get all the hamsters.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<HamsterDTO> findAll() {
        log.debug("Request to get all Hamsters");
        return hamsterRepository.findAll().stream()
            .map(hamsterMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one hamster by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public HamsterDTO findOne(Long id) {
        log.debug("Request to get Hamster : {}", id);
        Hamster hamster = hamsterRepository.findOne(id);
        return hamsterMapper.toDto(hamster);
    }

    /**
     * Delete the hamster by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Hamster : {}", id);
        hamsterRepository.nativeDelete(id);
    }
}
