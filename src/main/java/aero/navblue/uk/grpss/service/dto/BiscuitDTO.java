package aero.navblue.uk.grpss.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Biscuit entity.
 */
public class BiscuitDTO implements Serializable {

    private Long id;

    private String name;

    private Boolean chocolate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isChocolate() {
        return chocolate;
    }

    public void setChocolate(Boolean chocolate) {
        this.chocolate = chocolate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BiscuitDTO biscuitDTO = (BiscuitDTO) o;
        if(biscuitDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), biscuitDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BiscuitDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", chocolate='" + isChocolate() + "'" +
            "}";
    }
}
