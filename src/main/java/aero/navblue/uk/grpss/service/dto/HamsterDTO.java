package aero.navblue.uk.grpss.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Hamster entity.
 */
public class HamsterDTO implements Serializable {

    private Long id;

    private String name;

    private Double weight;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HamsterDTO hamsterDTO = (HamsterDTO) o;
        if(hamsterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), hamsterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HamsterDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", weight=" + getWeight() +
            "}";
    }
}
