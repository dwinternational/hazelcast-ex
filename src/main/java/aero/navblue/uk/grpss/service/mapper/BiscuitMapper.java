package aero.navblue.uk.grpss.service.mapper;

import aero.navblue.uk.grpss.domain.*;
import aero.navblue.uk.grpss.service.dto.BiscuitDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Biscuit and its DTO BiscuitDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BiscuitMapper extends EntityMapper<BiscuitDTO, Biscuit> {



    default Biscuit fromId(Long id) {
        if (id == null) {
            return null;
        }
        Biscuit biscuit = new Biscuit();
        biscuit.setId(id);
        return biscuit;
    }
}
