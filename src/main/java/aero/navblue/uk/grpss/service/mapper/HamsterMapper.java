package aero.navblue.uk.grpss.service.mapper;

import aero.navblue.uk.grpss.domain.*;
import aero.navblue.uk.grpss.service.dto.HamsterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Hamster and its DTO HamsterDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface HamsterMapper extends EntityMapper<HamsterDTO, Hamster> {



    default Hamster fromId(Long id) {
        if (id == null) {
            return null;
        }
        Hamster hamster = new Hamster();
        hamster.setId(id);
        return hamster;
    }
}
