package aero.navblue.uk.grpss.web.rest;

import com.codahale.metrics.annotation.Timed;
import aero.navblue.uk.grpss.service.BiscuitService;
import aero.navblue.uk.grpss.web.rest.errors.BadRequestAlertException;
import aero.navblue.uk.grpss.web.rest.util.HeaderUtil;
import aero.navblue.uk.grpss.web.rest.util.PaginationUtil;
import aero.navblue.uk.grpss.service.dto.BiscuitDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Biscuit.
 */
@RestController
@RequestMapping("/api")
public class BiscuitResource {

    private final Logger log = LoggerFactory.getLogger(BiscuitResource.class);

    private static final String ENTITY_NAME = "biscuit";

    private final BiscuitService biscuitService;

    public BiscuitResource(BiscuitService biscuitService) {
        this.biscuitService = biscuitService;
    }

    /**
     * POST  /biscuits : Create a new biscuit.
     *
     * @param biscuitDTO the biscuitDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new biscuitDTO, or with status 400 (Bad Request) if the biscuit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/biscuits")
    @Timed
    public ResponseEntity<BiscuitDTO> createBiscuit(@RequestBody BiscuitDTO biscuitDTO) throws URISyntaxException {
        log.debug("REST request to save Biscuit : {}", biscuitDTO);
        if (biscuitDTO.getId() != null) {
            throw new BadRequestAlertException("A new biscuit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BiscuitDTO result = biscuitService.save(biscuitDTO);
        return ResponseEntity.created(new URI("/api/biscuits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /biscuits : Updates an existing biscuit.
     *
     * @param biscuitDTO the biscuitDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated biscuitDTO,
     * or with status 400 (Bad Request) if the biscuitDTO is not valid,
     * or with status 500 (Internal Server Error) if the biscuitDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/biscuits")
    @Timed
    public ResponseEntity<BiscuitDTO> updateBiscuit(@RequestBody BiscuitDTO biscuitDTO) throws URISyntaxException {
        log.debug("REST request to update Biscuit : {}", biscuitDTO);
        if (biscuitDTO.getId() == null) {
            return createBiscuit(biscuitDTO);
        }
        BiscuitDTO result = biscuitService.save(biscuitDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, biscuitDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /biscuits : get all the biscuits.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of biscuits in body
     */
    @GetMapping("/biscuits")
    @Timed
    public ResponseEntity<List<BiscuitDTO>> getAllBiscuits(Pageable pageable) {
        log.debug("REST request to get a page of Biscuits");
        Page<BiscuitDTO> page = biscuitService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/biscuits");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /biscuits/:id : get the "id" biscuit.
     *
     * @param id the id of the biscuitDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the biscuitDTO, or with status 404 (Not Found)
     */
    @GetMapping("/biscuits/{id}")
    @Timed
    public ResponseEntity<BiscuitDTO> getBiscuit(@PathVariable Long id) {
        log.debug("REST request to get Biscuit : {}", id);
        BiscuitDTO biscuitDTO = biscuitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(biscuitDTO));
    }

    /**
     * DELETE  /biscuits/:id : delete the "id" biscuit.
     *
     * @param id the id of the biscuitDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/biscuits/{id}")
    @Timed
    public ResponseEntity<Void> deleteBiscuit(@PathVariable Long id) {
        log.debug("REST request to delete Biscuit : {}", id);
        biscuitService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
