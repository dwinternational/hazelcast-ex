package aero.navblue.uk.grpss.web.rest;

import com.codahale.metrics.annotation.Timed;
import aero.navblue.uk.grpss.service.HamsterService;
import aero.navblue.uk.grpss.web.rest.errors.BadRequestAlertException;
import aero.navblue.uk.grpss.web.rest.util.HeaderUtil;
import aero.navblue.uk.grpss.service.dto.HamsterDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Hamster.
 */
@RestController
@RequestMapping("/api")
public class HamsterResource {

    private final Logger log = LoggerFactory.getLogger(HamsterResource.class);

    private static final String ENTITY_NAME = "hamster";

    private final HamsterService hamsterService;

    public HamsterResource(HamsterService hamsterService) {
        this.hamsterService = hamsterService;
    }

    /**
     * POST  /hamsters : Create a new hamster.
     *
     * @param hamsterDTO the hamsterDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new hamsterDTO, or with status 400 (Bad Request) if the hamster has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/hamsters")
    @Timed
    public ResponseEntity<HamsterDTO> createHamster(@RequestBody HamsterDTO hamsterDTO) throws URISyntaxException {
        log.debug("REST request to save Hamster : {}", hamsterDTO);
        if (hamsterDTO.getId() != null) {
            throw new BadRequestAlertException("A new hamster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HamsterDTO result = hamsterService.save(hamsterDTO);
        return ResponseEntity.created(new URI("/api/hamsters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /hamsters : Updates an existing hamster.
     *
     * @param hamsterDTO the hamsterDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated hamsterDTO,
     * or with status 400 (Bad Request) if the hamsterDTO is not valid,
     * or with status 500 (Internal Server Error) if the hamsterDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/hamsters")
    @Timed
    public ResponseEntity<HamsterDTO> updateHamster(@RequestBody HamsterDTO hamsterDTO) throws URISyntaxException {
        log.debug("REST request to update Hamster : {}", hamsterDTO);
        if (hamsterDTO.getId() == null) {
            return createHamster(hamsterDTO);
        }
        HamsterDTO result = hamsterService.save(hamsterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, hamsterDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /hamsters : get all the hamsters.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of hamsters in body
     */
    @GetMapping("/hamsters")
    @Timed
    public List<HamsterDTO> getAllHamsters() {
        log.debug("REST request to get all Hamsters");
        return hamsterService.findAll();
        }

    /**
     * GET  /hamsters/:id : get the "id" hamster.
     *
     * @param id the id of the hamsterDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the hamsterDTO, or with status 404 (Not Found)
     */
    @GetMapping("/hamsters/{id}")
    @Timed
    public ResponseEntity<HamsterDTO> getHamster(@PathVariable Long id) {
        log.debug("REST request to get Hamster : {}", id);
        HamsterDTO hamsterDTO = hamsterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(hamsterDTO));
    }

    /**
     * DELETE  /hamsters/:id : delete the "id" hamster.
     *
     * @param id the id of the hamsterDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/hamsters/{id}")
    @Timed
    public ResponseEntity<Void> deleteHamster(@PathVariable Long id) {
        log.debug("REST request to delete Hamster : {}", id);
        hamsterService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
