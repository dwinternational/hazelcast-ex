/**
 * View Models used by Spring MVC REST controllers.
 */
package aero.navblue.uk.grpss.web.rest.vm;
