import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Biscuit } from './biscuit.model';
import { BiscuitPopupService } from './biscuit-popup.service';
import { BiscuitService } from './biscuit.service';

@Component({
    selector: 'jhi-biscuit-delete-dialog',
    templateUrl: './biscuit-delete-dialog.component.html'
})
export class BiscuitDeleteDialogComponent {

    biscuit: Biscuit;

    constructor(
        private biscuitService: BiscuitService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.biscuitService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'biscuitListModification',
                content: 'Deleted an biscuit'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-biscuit-delete-popup',
    template: ''
})
export class BiscuitDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private biscuitPopupService: BiscuitPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.biscuitPopupService
                .open(BiscuitDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
