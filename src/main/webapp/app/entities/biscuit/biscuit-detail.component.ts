import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Biscuit } from './biscuit.model';
import { BiscuitService } from './biscuit.service';

@Component({
    selector: 'jhi-biscuit-detail',
    templateUrl: './biscuit-detail.component.html'
})
export class BiscuitDetailComponent implements OnInit, OnDestroy {

    biscuit: Biscuit;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private biscuitService: BiscuitService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBiscuits();
    }

    load(id) {
        this.biscuitService.find(id)
            .subscribe((biscuitResponse: HttpResponse<Biscuit>) => {
                this.biscuit = biscuitResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBiscuits() {
        this.eventSubscriber = this.eventManager.subscribe(
            'biscuitListModification',
            (response) => this.load(this.biscuit.id)
        );
    }
}
