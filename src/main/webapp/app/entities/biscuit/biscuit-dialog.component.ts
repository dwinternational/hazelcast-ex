import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Biscuit } from './biscuit.model';
import { BiscuitPopupService } from './biscuit-popup.service';
import { BiscuitService } from './biscuit.service';

@Component({
    selector: 'jhi-biscuit-dialog',
    templateUrl: './biscuit-dialog.component.html'
})
export class BiscuitDialogComponent implements OnInit {

    biscuit: Biscuit;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private biscuitService: BiscuitService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.biscuit.id !== undefined) {
            this.subscribeToSaveResponse(
                this.biscuitService.update(this.biscuit));
        } else {
            this.subscribeToSaveResponse(
                this.biscuitService.create(this.biscuit));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Biscuit>>) {
        result.subscribe((res: HttpResponse<Biscuit>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Biscuit) {
        this.eventManager.broadcast({ name: 'biscuitListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-biscuit-popup',
    template: ''
})
export class BiscuitPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private biscuitPopupService: BiscuitPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.biscuitPopupService
                    .open(BiscuitDialogComponent as Component, params['id']);
            } else {
                this.biscuitPopupService
                    .open(BiscuitDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
