import { BaseEntity } from './../../shared';

export class Biscuit implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public chocolate?: boolean,
    ) {
        this.chocolate = false;
    }
}
