import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HazelcastexSharedModule } from '../../shared';
import {
    BiscuitService,
    BiscuitPopupService,
    BiscuitComponent,
    BiscuitDetailComponent,
    BiscuitDialogComponent,
    BiscuitPopupComponent,
    BiscuitDeletePopupComponent,
    BiscuitDeleteDialogComponent,
    biscuitRoute,
    biscuitPopupRoute,
    BiscuitResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...biscuitRoute,
    ...biscuitPopupRoute,
];

@NgModule({
    imports: [
        HazelcastexSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BiscuitComponent,
        BiscuitDetailComponent,
        BiscuitDialogComponent,
        BiscuitDeleteDialogComponent,
        BiscuitPopupComponent,
        BiscuitDeletePopupComponent,
    ],
    entryComponents: [
        BiscuitComponent,
        BiscuitDialogComponent,
        BiscuitPopupComponent,
        BiscuitDeleteDialogComponent,
        BiscuitDeletePopupComponent,
    ],
    providers: [
        BiscuitService,
        BiscuitPopupService,
        BiscuitResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HazelcastexBiscuitModule {}
