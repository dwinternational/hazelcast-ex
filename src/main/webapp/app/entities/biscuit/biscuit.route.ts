import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BiscuitComponent } from './biscuit.component';
import { BiscuitDetailComponent } from './biscuit-detail.component';
import { BiscuitPopupComponent } from './biscuit-dialog.component';
import { BiscuitDeletePopupComponent } from './biscuit-delete-dialog.component';

@Injectable()
export class BiscuitResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const biscuitRoute: Routes = [
    {
        path: 'biscuit',
        component: BiscuitComponent,
        resolve: {
            'pagingParams': BiscuitResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Biscuits'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'biscuit/:id',
        component: BiscuitDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Biscuits'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const biscuitPopupRoute: Routes = [
    {
        path: 'biscuit-new',
        component: BiscuitPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Biscuits'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'biscuit/:id/edit',
        component: BiscuitPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Biscuits'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'biscuit/:id/delete',
        component: BiscuitDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Biscuits'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
