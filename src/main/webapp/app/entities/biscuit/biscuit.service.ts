import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Biscuit } from './biscuit.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Biscuit>;

@Injectable()
export class BiscuitService {

    private resourceUrl =  SERVER_API_URL + 'api/biscuits';

    constructor(private http: HttpClient) { }

    create(biscuit: Biscuit): Observable<EntityResponseType> {
        const copy = this.convert(biscuit);
        return this.http.post<Biscuit>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(biscuit: Biscuit): Observable<EntityResponseType> {
        const copy = this.convert(biscuit);
        return this.http.put<Biscuit>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Biscuit>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Biscuit[]>> {
        const options = createRequestOption(req);
        return this.http.get<Biscuit[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Biscuit[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Biscuit = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Biscuit[]>): HttpResponse<Biscuit[]> {
        const jsonResponse: Biscuit[] = res.body;
        const body: Biscuit[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Biscuit.
     */
    private convertItemFromServer(biscuit: Biscuit): Biscuit {
        const copy: Biscuit = Object.assign({}, biscuit);
        return copy;
    }

    /**
     * Convert a Biscuit to a JSON which can be sent to the server.
     */
    private convert(biscuit: Biscuit): Biscuit {
        const copy: Biscuit = Object.assign({}, biscuit);
        return copy;
    }
}
