import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HazelcastexHamsterModule } from './hamster/hamster.module';
import { HazelcastexBiscuitModule } from './biscuit/biscuit.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        HazelcastexHamsterModule,
        HazelcastexBiscuitModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HazelcastexEntityModule {}
