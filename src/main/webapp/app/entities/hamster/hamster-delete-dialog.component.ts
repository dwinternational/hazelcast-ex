import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Hamster } from './hamster.model';
import { HamsterPopupService } from './hamster-popup.service';
import { HamsterService } from './hamster.service';

@Component({
    selector: 'jhi-hamster-delete-dialog',
    templateUrl: './hamster-delete-dialog.component.html'
})
export class HamsterDeleteDialogComponent {

    hamster: Hamster;

    constructor(
        private hamsterService: HamsterService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.hamsterService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'hamsterListModification',
                content: 'Deleted an hamster'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-hamster-delete-popup',
    template: ''
})
export class HamsterDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hamsterPopupService: HamsterPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.hamsterPopupService
                .open(HamsterDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
