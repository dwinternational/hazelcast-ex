import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Hamster } from './hamster.model';
import { HamsterService } from './hamster.service';

@Component({
    selector: 'jhi-hamster-detail',
    templateUrl: './hamster-detail.component.html'
})
export class HamsterDetailComponent implements OnInit, OnDestroy {

    hamster: Hamster;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private hamsterService: HamsterService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInHamsters();
    }

    load(id) {
        this.hamsterService.find(id)
            .subscribe((hamsterResponse: HttpResponse<Hamster>) => {
                this.hamster = hamsterResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInHamsters() {
        this.eventSubscriber = this.eventManager.subscribe(
            'hamsterListModification',
            (response) => this.load(this.hamster.id)
        );
    }
}
