import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Hamster } from './hamster.model';
import { HamsterPopupService } from './hamster-popup.service';
import { HamsterService } from './hamster.service';

@Component({
    selector: 'jhi-hamster-dialog',
    templateUrl: './hamster-dialog.component.html'
})
export class HamsterDialogComponent implements OnInit {

    hamster: Hamster;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private hamsterService: HamsterService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.hamster.id !== undefined) {
            this.subscribeToSaveResponse(
                this.hamsterService.update(this.hamster));
        } else {
            this.subscribeToSaveResponse(
                this.hamsterService.create(this.hamster));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Hamster>>) {
        result.subscribe((res: HttpResponse<Hamster>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Hamster) {
        this.eventManager.broadcast({ name: 'hamsterListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-hamster-popup',
    template: ''
})
export class HamsterPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private hamsterPopupService: HamsterPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.hamsterPopupService
                    .open(HamsterDialogComponent as Component, params['id']);
            } else {
                this.hamsterPopupService
                    .open(HamsterDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
