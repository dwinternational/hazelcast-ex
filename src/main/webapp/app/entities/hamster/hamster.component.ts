import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Hamster } from './hamster.model';
import { HamsterService } from './hamster.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-hamster',
    templateUrl: './hamster.component.html'
})
export class HamsterComponent implements OnInit, OnDestroy {
hamsters: Hamster[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private hamsterService: HamsterService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.hamsterService.query().subscribe(
            (res: HttpResponse<Hamster[]>) => {
                this.hamsters = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInHamsters();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Hamster) {
        return item.id;
    }
    registerChangeInHamsters() {
        this.eventSubscriber = this.eventManager.subscribe('hamsterListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
