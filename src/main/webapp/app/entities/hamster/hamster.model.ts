import { BaseEntity } from './../../shared';

export class Hamster implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public weight?: number,
    ) {
    }
}
