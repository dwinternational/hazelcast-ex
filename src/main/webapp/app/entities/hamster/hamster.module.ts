import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HazelcastexSharedModule } from '../../shared';
import {
    HamsterService,
    HamsterPopupService,
    HamsterComponent,
    HamsterDetailComponent,
    HamsterDialogComponent,
    HamsterPopupComponent,
    HamsterDeletePopupComponent,
    HamsterDeleteDialogComponent,
    hamsterRoute,
    hamsterPopupRoute,
} from './';

const ENTITY_STATES = [
    ...hamsterRoute,
    ...hamsterPopupRoute,
];

@NgModule({
    imports: [
        HazelcastexSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        HamsterComponent,
        HamsterDetailComponent,
        HamsterDialogComponent,
        HamsterDeleteDialogComponent,
        HamsterPopupComponent,
        HamsterDeletePopupComponent,
    ],
    entryComponents: [
        HamsterComponent,
        HamsterDialogComponent,
        HamsterPopupComponent,
        HamsterDeleteDialogComponent,
        HamsterDeletePopupComponent,
    ],
    providers: [
        HamsterService,
        HamsterPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HazelcastexHamsterModule {}
