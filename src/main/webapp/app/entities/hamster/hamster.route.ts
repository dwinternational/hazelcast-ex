import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { HamsterComponent } from './hamster.component';
import { HamsterDetailComponent } from './hamster-detail.component';
import { HamsterPopupComponent } from './hamster-dialog.component';
import { HamsterDeletePopupComponent } from './hamster-delete-dialog.component';

export const hamsterRoute: Routes = [
    {
        path: 'hamster',
        component: HamsterComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Hamsters'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'hamster/:id',
        component: HamsterDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Hamsters'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const hamsterPopupRoute: Routes = [
    {
        path: 'hamster-new',
        component: HamsterPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Hamsters'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'hamster/:id/edit',
        component: HamsterPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Hamsters'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'hamster/:id/delete',
        component: HamsterDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Hamsters'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
