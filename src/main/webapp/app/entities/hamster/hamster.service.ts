import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Hamster } from './hamster.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Hamster>;

@Injectable()
export class HamsterService {

    private resourceUrl =  SERVER_API_URL + 'api/hamsters';

    constructor(private http: HttpClient) { }

    create(hamster: Hamster): Observable<EntityResponseType> {
        const copy = this.convert(hamster);
        return this.http.post<Hamster>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(hamster: Hamster): Observable<EntityResponseType> {
        const copy = this.convert(hamster);
        return this.http.put<Hamster>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Hamster>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Hamster[]>> {
        const options = createRequestOption(req);
        return this.http.get<Hamster[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Hamster[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Hamster = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Hamster[]>): HttpResponse<Hamster[]> {
        const jsonResponse: Hamster[] = res.body;
        const body: Hamster[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Hamster.
     */
    private convertItemFromServer(hamster: Hamster): Hamster {
        const copy: Hamster = Object.assign({}, hamster);
        return copy;
    }

    /**
     * Convert a Hamster to a JSON which can be sent to the server.
     */
    private convert(hamster: Hamster): Hamster {
        const copy: Hamster = Object.assign({}, hamster);
        return copy;
    }
}
