package aero.navblue.uk.grpss.web.rest;

import aero.navblue.uk.grpss.HazelcastexApp;

import aero.navblue.uk.grpss.domain.Biscuit;
import aero.navblue.uk.grpss.repository.BiscuitRepository;
import aero.navblue.uk.grpss.service.BiscuitService;
import aero.navblue.uk.grpss.service.dto.BiscuitDTO;
import aero.navblue.uk.grpss.service.mapper.BiscuitMapper;
import aero.navblue.uk.grpss.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static aero.navblue.uk.grpss.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BiscuitResource REST controller.
 *
 * @see BiscuitResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HazelcastexApp.class)
public class BiscuitResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CHOCOLATE = false;
    private static final Boolean UPDATED_CHOCOLATE = true;

    @Autowired
    private BiscuitRepository biscuitRepository;

    @Autowired
    private BiscuitMapper biscuitMapper;

    @Autowired
    private BiscuitService biscuitService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBiscuitMockMvc;

    private Biscuit biscuit;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BiscuitResource biscuitResource = new BiscuitResource(biscuitService);
        this.restBiscuitMockMvc = MockMvcBuilders.standaloneSetup(biscuitResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Biscuit createEntity(EntityManager em) {
        Biscuit biscuit = new Biscuit()
            .name(DEFAULT_NAME)
            .chocolate(DEFAULT_CHOCOLATE);
        return biscuit;
    }

    @Before
    public void initTest() {
        biscuit = createEntity(em);
    }

    @Test
    @Transactional
    public void createBiscuit() throws Exception {
        int databaseSizeBeforeCreate = biscuitRepository.findAll().size();

        // Create the Biscuit
        BiscuitDTO biscuitDTO = biscuitMapper.toDto(biscuit);
        restBiscuitMockMvc.perform(post("/api/biscuits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(biscuitDTO)))
            .andExpect(status().isCreated());

        // Validate the Biscuit in the database
        List<Biscuit> biscuitList = biscuitRepository.findAll();
        assertThat(biscuitList).hasSize(databaseSizeBeforeCreate + 1);
        Biscuit testBiscuit = biscuitList.get(biscuitList.size() - 1);
        assertThat(testBiscuit.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testBiscuit.isChocolate()).isEqualTo(DEFAULT_CHOCOLATE);
    }

    @Test
    @Transactional
    public void createBiscuitWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = biscuitRepository.findAll().size();

        // Create the Biscuit with an existing ID
        biscuit.setId(1L);
        BiscuitDTO biscuitDTO = biscuitMapper.toDto(biscuit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBiscuitMockMvc.perform(post("/api/biscuits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(biscuitDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Biscuit in the database
        List<Biscuit> biscuitList = biscuitRepository.findAll();
        assertThat(biscuitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBiscuits() throws Exception {
        // Initialize the database
        biscuitRepository.saveAndFlush(biscuit);

        // Get all the biscuitList
        restBiscuitMockMvc.perform(get("/api/biscuits?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(biscuit.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].chocolate").value(hasItem(DEFAULT_CHOCOLATE.booleanValue())));
    }

    @Test
    @Transactional
    public void getBiscuit() throws Exception {
        // Initialize the database
        biscuitRepository.saveAndFlush(biscuit);

        // Get the biscuit
        restBiscuitMockMvc.perform(get("/api/biscuits/{id}", biscuit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(biscuit.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.chocolate").value(DEFAULT_CHOCOLATE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingBiscuit() throws Exception {
        // Get the biscuit
        restBiscuitMockMvc.perform(get("/api/biscuits/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBiscuit() throws Exception {
        // Initialize the database
        biscuitRepository.saveAndFlush(biscuit);
        int databaseSizeBeforeUpdate = biscuitRepository.findAll().size();

        // Update the biscuit
        Biscuit updatedBiscuit = biscuitRepository.findOne(biscuit.getId());
        // Disconnect from session so that the updates on updatedBiscuit are not directly saved in db
        em.detach(updatedBiscuit);
        updatedBiscuit
            .name(UPDATED_NAME)
            .chocolate(UPDATED_CHOCOLATE);
        BiscuitDTO biscuitDTO = biscuitMapper.toDto(updatedBiscuit);

        restBiscuitMockMvc.perform(put("/api/biscuits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(biscuitDTO)))
            .andExpect(status().isOk());

        // Validate the Biscuit in the database
        List<Biscuit> biscuitList = biscuitRepository.findAll();
        assertThat(biscuitList).hasSize(databaseSizeBeforeUpdate);
        Biscuit testBiscuit = biscuitList.get(biscuitList.size() - 1);
        assertThat(testBiscuit.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testBiscuit.isChocolate()).isEqualTo(UPDATED_CHOCOLATE);
    }

    @Test
    @Transactional
    public void updateNonExistingBiscuit() throws Exception {
        int databaseSizeBeforeUpdate = biscuitRepository.findAll().size();

        // Create the Biscuit
        BiscuitDTO biscuitDTO = biscuitMapper.toDto(biscuit);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBiscuitMockMvc.perform(put("/api/biscuits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(biscuitDTO)))
            .andExpect(status().isCreated());

        // Validate the Biscuit in the database
        List<Biscuit> biscuitList = biscuitRepository.findAll();
        assertThat(biscuitList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteBiscuit() throws Exception {
        // Initialize the database
        biscuitRepository.saveAndFlush(biscuit);
        int databaseSizeBeforeDelete = biscuitRepository.findAll().size();

        // Get the biscuit
        restBiscuitMockMvc.perform(delete("/api/biscuits/{id}", biscuit.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Biscuit> biscuitList = biscuitRepository.findAll();
        assertThat(biscuitList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Biscuit.class);
        Biscuit biscuit1 = new Biscuit();
        biscuit1.setId(1L);
        Biscuit biscuit2 = new Biscuit();
        biscuit2.setId(biscuit1.getId());
        assertThat(biscuit1).isEqualTo(biscuit2);
        biscuit2.setId(2L);
        assertThat(biscuit1).isNotEqualTo(biscuit2);
        biscuit1.setId(null);
        assertThat(biscuit1).isNotEqualTo(biscuit2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BiscuitDTO.class);
        BiscuitDTO biscuitDTO1 = new BiscuitDTO();
        biscuitDTO1.setId(1L);
        BiscuitDTO biscuitDTO2 = new BiscuitDTO();
        assertThat(biscuitDTO1).isNotEqualTo(biscuitDTO2);
        biscuitDTO2.setId(biscuitDTO1.getId());
        assertThat(biscuitDTO1).isEqualTo(biscuitDTO2);
        biscuitDTO2.setId(2L);
        assertThat(biscuitDTO1).isNotEqualTo(biscuitDTO2);
        biscuitDTO1.setId(null);
        assertThat(biscuitDTO1).isNotEqualTo(biscuitDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(biscuitMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(biscuitMapper.fromId(null)).isNull();
    }
}
