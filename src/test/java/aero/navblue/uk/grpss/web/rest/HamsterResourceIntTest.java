package aero.navblue.uk.grpss.web.rest;

import aero.navblue.uk.grpss.HazelcastexApp;

import aero.navblue.uk.grpss.domain.Hamster;
import aero.navblue.uk.grpss.repository.HamsterRepository;
import aero.navblue.uk.grpss.service.HamsterService;
import aero.navblue.uk.grpss.service.dto.HamsterDTO;
import aero.navblue.uk.grpss.service.mapper.HamsterMapper;
import aero.navblue.uk.grpss.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static aero.navblue.uk.grpss.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the HamsterResource REST controller.
 *
 * @see HamsterResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HazelcastexApp.class)
public class HamsterResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Double DEFAULT_WEIGHT = 1D;
    private static final Double UPDATED_WEIGHT = 2D;

    @Autowired
    private HamsterRepository hamsterRepository;

    @Autowired
    private HamsterMapper hamsterMapper;

    @Autowired
    private HamsterService hamsterService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restHamsterMockMvc;

    private Hamster hamster;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HamsterResource hamsterResource = new HamsterResource(hamsterService);
        this.restHamsterMockMvc = MockMvcBuilders.standaloneSetup(hamsterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hamster createEntity(EntityManager em) {
        Hamster hamster = new Hamster()
            .name(DEFAULT_NAME)
            .weight(DEFAULT_WEIGHT);
        return hamster;
    }

    @Before
    public void initTest() {
        hamster = createEntity(em);
    }

    @Test
    @Transactional
    public void createHamster() throws Exception {
        int databaseSizeBeforeCreate = hamsterRepository.findAll().size();

        // Create the Hamster
        HamsterDTO hamsterDTO = hamsterMapper.toDto(hamster);
        restHamsterMockMvc.perform(post("/api/hamsters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hamsterDTO)))
            .andExpect(status().isCreated());

        // Validate the Hamster in the database
        List<Hamster> hamsterList = hamsterRepository.findAll();
        assertThat(hamsterList).hasSize(databaseSizeBeforeCreate + 1);
        Hamster testHamster = hamsterList.get(hamsterList.size() - 1);
        assertThat(testHamster.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHamster.getWeight()).isEqualTo(DEFAULT_WEIGHT);
    }

    @Test
    @Transactional
    public void createHamsterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = hamsterRepository.findAll().size();

        // Create the Hamster with an existing ID
        hamster.setId(1L);
        HamsterDTO hamsterDTO = hamsterMapper.toDto(hamster);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHamsterMockMvc.perform(post("/api/hamsters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hamsterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Hamster in the database
        List<Hamster> hamsterList = hamsterRepository.findAll();
        assertThat(hamsterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllHamsters() throws Exception {
        // Initialize the database
        hamsterRepository.saveAndFlush(hamster);

        // Get all the hamsterList
        restHamsterMockMvc.perform(get("/api/hamsters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hamster.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT.doubleValue())));
    }

    @Test
    @Transactional
    public void getHamster() throws Exception {
        // Initialize the database
        hamsterRepository.saveAndFlush(hamster);

        // Get the hamster
        restHamsterMockMvc.perform(get("/api/hamsters/{id}", hamster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(hamster.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.weight").value(DEFAULT_WEIGHT.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingHamster() throws Exception {
        // Get the hamster
        restHamsterMockMvc.perform(get("/api/hamsters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHamster() throws Exception {
        // Initialize the database
        hamsterRepository.saveAndFlush(hamster);
        int databaseSizeBeforeUpdate = hamsterRepository.findAll().size();

        // Update the hamster
        Hamster updatedHamster = hamsterRepository.findOne(hamster.getId());
        // Disconnect from session so that the updates on updatedHamster are not directly saved in db
        em.detach(updatedHamster);
        updatedHamster
            .name(UPDATED_NAME)
            .weight(UPDATED_WEIGHT);
        HamsterDTO hamsterDTO = hamsterMapper.toDto(updatedHamster);

        restHamsterMockMvc.perform(put("/api/hamsters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hamsterDTO)))
            .andExpect(status().isOk());

        // Validate the Hamster in the database
        List<Hamster> hamsterList = hamsterRepository.findAll();
        assertThat(hamsterList).hasSize(databaseSizeBeforeUpdate);
        Hamster testHamster = hamsterList.get(hamsterList.size() - 1);
        assertThat(testHamster.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHamster.getWeight()).isEqualTo(UPDATED_WEIGHT);
    }

    @Test
    @Transactional
    public void updateNonExistingHamster() throws Exception {
        int databaseSizeBeforeUpdate = hamsterRepository.findAll().size();

        // Create the Hamster
        HamsterDTO hamsterDTO = hamsterMapper.toDto(hamster);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restHamsterMockMvc.perform(put("/api/hamsters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(hamsterDTO)))
            .andExpect(status().isCreated());

        // Validate the Hamster in the database
        List<Hamster> hamsterList = hamsterRepository.findAll();
        assertThat(hamsterList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteHamster() throws Exception {
        // Initialize the database
        hamsterRepository.saveAndFlush(hamster);
        int databaseSizeBeforeDelete = hamsterRepository.findAll().size();

        // Get the hamster
        restHamsterMockMvc.perform(delete("/api/hamsters/{id}", hamster.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Hamster> hamsterList = hamsterRepository.findAll();
        assertThat(hamsterList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Hamster.class);
        Hamster hamster1 = new Hamster();
        hamster1.setId(1L);
        Hamster hamster2 = new Hamster();
        hamster2.setId(hamster1.getId());
        assertThat(hamster1).isEqualTo(hamster2);
        hamster2.setId(2L);
        assertThat(hamster1).isNotEqualTo(hamster2);
        hamster1.setId(null);
        assertThat(hamster1).isNotEqualTo(hamster2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HamsterDTO.class);
        HamsterDTO hamsterDTO1 = new HamsterDTO();
        hamsterDTO1.setId(1L);
        HamsterDTO hamsterDTO2 = new HamsterDTO();
        assertThat(hamsterDTO1).isNotEqualTo(hamsterDTO2);
        hamsterDTO2.setId(hamsterDTO1.getId());
        assertThat(hamsterDTO1).isEqualTo(hamsterDTO2);
        hamsterDTO2.setId(2L);
        assertThat(hamsterDTO1).isNotEqualTo(hamsterDTO2);
        hamsterDTO1.setId(null);
        assertThat(hamsterDTO1).isNotEqualTo(hamsterDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(hamsterMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(hamsterMapper.fromId(null)).isNull();
    }
}
