/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { HazelcastexTestModule } from '../../../test.module';
import { BiscuitDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/biscuit/biscuit-delete-dialog.component';
import { BiscuitService } from '../../../../../../main/webapp/app/entities/biscuit/biscuit.service';

describe('Component Tests', () => {

    describe('Biscuit Management Delete Component', () => {
        let comp: BiscuitDeleteDialogComponent;
        let fixture: ComponentFixture<BiscuitDeleteDialogComponent>;
        let service: BiscuitService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HazelcastexTestModule],
                declarations: [BiscuitDeleteDialogComponent],
                providers: [
                    BiscuitService
                ]
            })
            .overrideTemplate(BiscuitDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BiscuitDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BiscuitService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
