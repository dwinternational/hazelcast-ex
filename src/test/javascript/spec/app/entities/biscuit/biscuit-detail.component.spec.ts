/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HazelcastexTestModule } from '../../../test.module';
import { BiscuitDetailComponent } from '../../../../../../main/webapp/app/entities/biscuit/biscuit-detail.component';
import { BiscuitService } from '../../../../../../main/webapp/app/entities/biscuit/biscuit.service';
import { Biscuit } from '../../../../../../main/webapp/app/entities/biscuit/biscuit.model';

describe('Component Tests', () => {

    describe('Biscuit Management Detail Component', () => {
        let comp: BiscuitDetailComponent;
        let fixture: ComponentFixture<BiscuitDetailComponent>;
        let service: BiscuitService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HazelcastexTestModule],
                declarations: [BiscuitDetailComponent],
                providers: [
                    BiscuitService
                ]
            })
            .overrideTemplate(BiscuitDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BiscuitDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BiscuitService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Biscuit(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.biscuit).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
