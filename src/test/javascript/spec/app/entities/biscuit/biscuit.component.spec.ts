/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HazelcastexTestModule } from '../../../test.module';
import { BiscuitComponent } from '../../../../../../main/webapp/app/entities/biscuit/biscuit.component';
import { BiscuitService } from '../../../../../../main/webapp/app/entities/biscuit/biscuit.service';
import { Biscuit } from '../../../../../../main/webapp/app/entities/biscuit/biscuit.model';

describe('Component Tests', () => {

    describe('Biscuit Management Component', () => {
        let comp: BiscuitComponent;
        let fixture: ComponentFixture<BiscuitComponent>;
        let service: BiscuitService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HazelcastexTestModule],
                declarations: [BiscuitComponent],
                providers: [
                    BiscuitService
                ]
            })
            .overrideTemplate(BiscuitComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BiscuitComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BiscuitService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Biscuit(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.biscuits[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
