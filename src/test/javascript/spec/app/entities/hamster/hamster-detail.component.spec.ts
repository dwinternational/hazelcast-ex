/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { HazelcastexTestModule } from '../../../test.module';
import { HamsterDetailComponent } from '../../../../../../main/webapp/app/entities/hamster/hamster-detail.component';
import { HamsterService } from '../../../../../../main/webapp/app/entities/hamster/hamster.service';
import { Hamster } from '../../../../../../main/webapp/app/entities/hamster/hamster.model';

describe('Component Tests', () => {

    describe('Hamster Management Detail Component', () => {
        let comp: HamsterDetailComponent;
        let fixture: ComponentFixture<HamsterDetailComponent>;
        let service: HamsterService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HazelcastexTestModule],
                declarations: [HamsterDetailComponent],
                providers: [
                    HamsterService
                ]
            })
            .overrideTemplate(HamsterDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(HamsterDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(HamsterService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Hamster(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.hamster).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
