/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { HazelcastexTestModule } from '../../../test.module';
import { HamsterComponent } from '../../../../../../main/webapp/app/entities/hamster/hamster.component';
import { HamsterService } from '../../../../../../main/webapp/app/entities/hamster/hamster.service';
import { Hamster } from '../../../../../../main/webapp/app/entities/hamster/hamster.model';

describe('Component Tests', () => {

    describe('Hamster Management Component', () => {
        let comp: HamsterComponent;
        let fixture: ComponentFixture<HamsterComponent>;
        let service: HamsterService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HazelcastexTestModule],
                declarations: [HamsterComponent],
                providers: [
                    HamsterService
                ]
            })
            .overrideTemplate(HamsterComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(HamsterComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(HamsterService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Hamster(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.hamsters[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
